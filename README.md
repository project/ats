CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Drupal ATS project is an open-source Applicant Tracking System built
on Drupal, and sponsored by Esteemed.

Applicant Tracking Systems are closely related to Customer Relationship
Management (CRM) systems, but they are tuned to facilitate a screening and
hiring workflow.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ats

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ats

REQUIREMENTS
------------

This project requires the following modules:

 * [Entity API](https://www.drupal.org/project/entity)

RECOMMENDED MODULES
-------------------

Placeholder / to be determined

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

Placeholder / to be determined

TROUBLESHOOTING
---------------

Placeholder / to be determined

FAQ
---

Placeholder / to be determined

MAINTAINERS
-----------

Current maintainers:
 * Albert Volkman (albert-volkman) - https://www.drupal.org/u/albert-volkman
 * Matt Obert (hotwebmatter) - https://www.drupal.org/u/hotwebmatter
 * Aleksandr Novolokov (a.novolokov) - https://www.drupal.org/u/anovolokov

This project has been sponsored by:
 * ESTEEMED
   Visit https://esteemed.io for more information.
