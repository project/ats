<?php

namespace Drupal\ats_candidate\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the ats candidate entity edit forms.
 */
class AtsCandidateForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New Candidate %label has been created.', $message_arguments));
      $this->logger('ats_candidate')->notice('Created new ats candidate %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The Candidate %label has been updated.', $message_arguments));
      $this->logger('ats_candidate')->notice('Updated new Candidate %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.ats_candidate.canonical', ['ats_candidate' => $entity->id()]);
  }

}
