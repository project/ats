<?php

namespace Drupal\ats_candidate\Entity;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\ats_candidate\AtsCandidateInterface;
use Drupal\user\UserInterface;

/**
 * Defines the ats candidate entity class.
 *
 * @ContentEntityType(
 *   id = "ats_candidate",
 *   label = @Translation("Candidate"),
 *   label_collection = @Translation("Candidates"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ats_candidate\AtsCandidateListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\ats_candidate\Form\AtsCandidateForm",
 *       "edit" = "Drupal\ats_candidate\Form\AtsCandidateForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "ats_candidate",
 *   admin_permission = "administer ats candidate",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "ats_can_user" = "ats_can_user"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/ats-candidate/add",
 *     "canonical" = "/ats_candidate/{ats_candidate}",
 *     "edit-form" = "/admin/content/ats-candidate/{ats_candidate}/edit",
 *     "delete-form" = "/admin/content/ats-candidate/{ats_candidate}/delete",
 *     "collection" = "/admin/content/ats-candidate"
 *   },
 *   field_ui_base_route = "entity.ats_candidate.settings"
 * )
 */
class AtsCandidate extends ContentEntityBase implements AtsCandidateInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new ats candidate entity is created, set the ats_can_ownership
   * entity reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['ats_can_ownership' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getFullName();
  }

  /**
   * {@inheritdoc}
   */
  public function getFullName() {
    $first_name = $this->get('ats_can_first_name')->value;
    $middle_name = $this->get('ats_can_middle_name')->value;
    $last_name = $this->get('ats_can_last_name')->value;
    $name = $first_name . (empty($middle_name) ? '' : ' ') . $middle_name . (empty($first_name) ? '' : ' ') . $last_name;
    // Allow other modules to alter the full name of the contact.
    \Drupal::moduleHandler()->alter('ats_candidate_name', $name, $this);
    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($status) {
    $this->set('active', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('ats_can_user')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('ats_can_user')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['ats_can_first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First Name'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_middle_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Middle Name'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last Name'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_mobile'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mobile Phone'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_date_available'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date Available'))
      ->setDefaultValue('')
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_compensation'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Compensation'))
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler', 'default:paragraph')
      ->setSetting('handler_settings', ['target_bundles' => ['ats_can_compensation' => 'ats_can_compensation']])
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_paragraphs',
        'weight' => 5,
        'settings' => [
          'title' => 'Compensation',
          'title_plural' => 'Compensation',
          'edit_mode' => 'open',
          'add_mode' => 'dropdown',
          'form_display_mode' => 'default',
          'default_paragraph_type' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['ats_can_status'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Candidate Status'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_candidate_status' => 'ats_candidate_status',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_employee_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Employee Type'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_employee_type' => 'ats_employee_type',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_employee_pref_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Employee Preferred Type'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_employee_type' => 'ats_employee_type',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_source'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Source'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_candidate_source' => 'ats_candidate_source',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_experience_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Experience'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_candidate_experience' => 'ats_candidate_experience',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_skills'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Skills'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_candidate_skills' => 'ats_candidate_skills',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'placeholder' => '',
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_education'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Education'))
      ->setCardinality(-1)
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler', 'default:paragraph')
      ->setSetting('handler_settings', ['target_bundles' => ['ats_can_education' => 'ats_can_education']])
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_paragraphs',
        'weight' => 11,
        'settings' => [
          'title' => 'Education',
          'title_plural' => 'Educations',
          'edit_mode' => 'open',
          'add_mode' => 'dropdown',
          'form_display_mode' => 'default',
          'default_paragraph_type' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_certifications'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Certifications'))
      ->setCardinality(-1)
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler', 'default:paragraph')
      ->setSetting('handler_settings', ['target_bundles' => ['ats_can_certifications' => 'ats_can_certifications']])
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_paragraphs',
        'weight' => 12,
        'settings' => [
          'title' => 'Certification',
          'title_plural' => 'Certifications',
          'edit_mode' => 'open',
          'add_mode' => 'dropdown',
          'form_display_mode' => 'default',
          'default_paragraph_type' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_address'] = BaseFieldDefinition::create('address')
      ->setLabel(t('Address'))
      ->setSetting('field_overrides', [
        AddressField::GIVEN_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDITIONAL_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::FAMILY_NAME => ['override' => FieldOverride::HIDDEN],
        AddressField::ORGANIZATION => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDRESS_LINE1 => ['override' => FieldOverride::HIDDEN],
        AddressField::ADDRESS_LINE2 => ['override' => FieldOverride::HIDDEN],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'address_default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'address_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_volunteering'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Volunteering'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_candidate_volunteering' => 'ats_candidate_volunteering',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_interests'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Hobbies / Interests'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'ats_candidate_interests' => 'ats_candidate_interests',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_linkedin'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Linkedin Profile URL'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_resume'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Resume'))
      ->setDescription(t('Candidate\'s resume.'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default:node')
      ->setSetting('handler_settings', [
        'target_bundles' => ['resume' => 'resume'],
        'auto_create' => TRUE,
      ])
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_modal_widget',
        'weight' => 16,
        'settings' => [
          'allow_add_new' => 1,
          'selected_bundle' => 'resume',
          'modal_title' => 'Add new Resume',
          'add_new_button_title' => 'Add new Resume',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['ats_can_file'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Files'))
      ->setDescription(t('Candidate\'s files.'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default:node')
      ->setSetting('handler_settings', [
        'target_bundles' => ['file' => 'file'],
        'auto_create' => TRUE,
      ])
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_modal_widget',
        'weight' => 17,
        'settings' => [
          'allow_add_new' => 1,
          'selected_bundle' => 'file',
          'modal_title' => 'Add new File',
          'add_new_button_title' => 'Add new File',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['ats_can_notes'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Notes'))
      ->setDescription(t("Candidate's notes."))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default:node')
      ->setSetting('handler_settings', [
        'target_bundles' => ['note' => 'note'],
        'auto_create' => TRUE,
      ])
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_modal_widget',
        'weight' => 18,
        'settings' => [
          'allow_add_new' => 1,
          'selected_bundle' => 'note',
          'modal_title' => 'Add new Note',
          'add_new_button_title' => 'Add new Note',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['ats_can_task'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tasks'))
      ->setDescription(t("Candidate's task."))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default:node')
      ->setSetting('handler_settings', [
        'target_bundles' => ['task' => 'task'],
        'auto_create' => TRUE,
      ])
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_modal_widget',
        'weight' => 19,
        'settings' => [
          'allow_add_new' => 1,
          'selected_bundle' => 'task',
          'modal_title' => 'Add new Task',
          'add_new_button_title' => 'Add new Task',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['ats_can_user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Candidate User ID'))
      ->setDescription(t('The user ID of the ATS Candidate.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_modal_widget',
        'weight' => 20,
        'settings' => [
          'allow_add_new' => 1,
          'selected_bundle' => 'user',
          'modal_title' => 'Add new Candidate User',
          'add_new_button_title' => 'Add new Candidate User',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('A boolean indicating whether the ATS Candidate is active.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => -20,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['ats_can_ownership'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Ownership'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the ATS Candidate was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the ATS Candidate was last edited.'));

    return $fields;
  }

}
