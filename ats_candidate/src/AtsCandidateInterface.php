<?php

namespace Drupal\ats_candidate;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an ATS Candidate entity type.
 */
interface AtsCandidateInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the ATS Candidate creation timestamp.
   *
   * @return int
   *   Creation timestamp of the ats candidate.
   */
  public function getCreatedTime();

  /**
   * Sets the ATS Candidate creation timestamp.
   *
   * @param int $timestamp
   *   The ats candidate creation timestamp.
   *
   * @return \Drupal\ats_candidate\AtsCandidateInterface
   *   The called ats candidate entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns a label for the ATS Candidate.
   */
  public function label();

    /**
   * Returns the full name of the ATS Candidate.
   *
   * @return string
   *   Name of the Candidate.
   */
  public function getFullName();

  /**
   * Returns the ATS Candidate active status indicator.
   *
   * @return bool
   *   TRUE if the Candidate is active.
   */
  public function isActive();

  /**
   * Sets the active atatus of an ATS Candidate.
   *
   * @param bool $active
   *   TRUE to set this Contact to active, FALSE to set it to inactive.
   *
   * @return \Drupal\ats_candidate\AtsCandidateInterface
   *   The called ATS Candidate entity.
   */
  public function setActive($active);

}
