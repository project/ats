<?php

/**
 * @file
 * Describes API functions for the ATS Candidate module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the display name for a candidate.
 *
 * @param string $name
 *   The generated name.
 * @param Drupal\ats_candidate\CandidateInterface $candidate
 *   The ATS Candidate whose name is being generated.
 *
 * @return string
 */
function hook_ats_candidate_name_alter(&$name, Drupal\ats_candidate\CandidateInterface $candidate) {
  return $candidate->get('ats_can_last_name')->value . ', ' . $candidate->get('ats_can_first_name')->value;
}

/**
 * @} End of "addtogroup hooks".
 */

